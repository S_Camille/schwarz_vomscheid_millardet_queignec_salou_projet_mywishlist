<?php

namespace mywishlist\models;

class MessageListe extends \Illuminate\Database\Eloquent\Model {

	protected $table = 'message-liste';
	protected $primaryKey = 'mess_id';
	public $timestamps = false;
	
	public function liste() {
		return $this->belongsTo('mywishlist\models\Liste', 'liste_id');
	}
	
	public function utilisateur() {
		return $this->belongsTo('mywishlist\models\Utilisateur', 'user_id');
	}
	
}