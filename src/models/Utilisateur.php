<?php

namespace mywishlist\models;

class Utilisateur extends \Illuminate\Database\Eloquent\Model {

	protected $table = 'utilisateur';
	protected $primaryKey = 'user_id';
	public $timestamps = false;
	
	public function listes() {
		return $this->hasMany('mywishlist\models\Liste', 'user_id');
	}
	
	public function messages() {
		return $this->hasMany('mywishlist\models\MessageListe', 'user_id');
	}
	
	public function role() {
		return $this->belongsTo('mywishlist\models\Role', 'role_id');
	}
	
	public function cagnotte(){
    	return $this->HasMany('mywishlist\models\Cagnotte', 'user_id');
	}
	
}