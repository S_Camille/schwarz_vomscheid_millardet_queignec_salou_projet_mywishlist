<?php

namespace mywishlist\models;

class Cagnotte extends \Illuminate\Database\Eloquent\Model {

	protected $table = 'coll-cagnotte';
	protected $primaryKey = 'cagn_id';
	public $timestamps = false;

	public function item() {
		return $this->BelongsTo('mywishlist\models\Item', 'item_id');
	}
	
	public function utilisateur() {
		return $this->belongsTo('mywishlist\models\Utilisateur', 'user_id');
	}

}