<?php

namespace mywishlist\models;

class Liste extends \Illuminate\Database\Eloquent\Model {

	protected $table = 'liste';
	protected $primaryKey = 'no';
	public $timestamps = false;
	
	public function items() {
		return $this->hasMany('mywishlist\models\Item', 'liste_id');
	}
	
	public function messages() {
		return $this->hasMany('mywishlist\models\MessageListe', 'liste_id');
	}
	
	public function utilisateur() {
		return $this->belongsTo('mywishlist\models\Utilisateur', 'user_id');
	}
	
}