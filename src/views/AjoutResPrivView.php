<?php

namespace mywishlist\views;

use mywishlist\models\Item;
use mywishlist\models\Liste;
use mywishlist\models\Reservation;

class AjoutResPrivView {

    private $id, $user_id;
    
	public function __construct($token) {
    	$this->id = Liste::where('token', '=', $token)->first()->no;
    	$this->user_id = Liste::where('token', '=', $token)->first()->user_id;
	}

	public function render() {
		$html = "";
        $it = Item::where('id_reserv', '!=', NULL)->where('liste_id', '=', $this->id)->get();
        if ($it->count() != 0){
            $html = $html . "Pour la liste n° ".$this->id.", des items sont réservés :</br>";
        }
		else
			$html = $html . "Aucun item n'est réservé.";
        foreach ($it as $item) {
            $html = $html."		- L'Item n°".$item->id.", qui est ".$item->nom." est réservé";
            if (isset($_COOKIE['createur']) && $_COOKIE['createur'] == $this->user_id) {
                $html = $html.".</br>";
            }
            else {
                $res = Reservation::where('id_reserv', '=', $item->id_reserv)->first();
                $html = $html." par ".$res->nom_reserv.".</br>";
            }
        }
		return $html;
	}
}