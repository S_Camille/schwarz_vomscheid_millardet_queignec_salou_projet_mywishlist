<?php

namespace mywishlist\views;

use mywishlist\models\Liste;

class AjoutMessListeView {
	
	public function __construct() {}

 	public static function render() {
		$app = \Slim\Slim::getInstance();
     	$url = $app->urlFor('AddMessListe').'?token='.$_GET['no'];
     	$peutCommenter = isset($_SESSION['user_connected']);
		$placeholder = '"Écrivez votre commentaire ici"';
		$disabled='';
		if (!$peutCommenter) {
			$placeholder = '"Connectez-vous pour pouvoir commenter."';
			$disabled = 'disabled';
		}
		$html = <<<END
<form method="POST" action="$url" id="ajoutMessListe">
	<label for="addcommentaire">Poster un commentaire</label></br>
	<textarea name="commentaire" id="addcommentaire" placeholder=$placeholder$disabled required rows="4" cols="40"></textarea></br>
	<button type="submit" name="ajouter_messListe" value="ajout_messListe"$disabled >Ajouter un commentaire</button>
</form>
END;
		return $html;
	}
	
}