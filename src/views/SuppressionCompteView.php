<?php

namespace mywishlist\views;

class SuppressionCompteView {

	private $id;
	
 	public function __construct($id) {
		$this->id=$id;
	}

	public function render() {
		$app=\Slim\Slim::getInstance();
		$url = $app->urlFor('SuppressionTotale');
		$html = <<<END
			<div id="form">
				<p class = "center">Pour supprimer votre compte, veuillez remplir les champs ci-dessous. Toute suppression de compte est irréversible et entraîne la suppresion des listes et items qui lui sont associés !</p>
				<form method="POST" action="$url">
					<p class = "formu">
						<label for="email">Adresse email : </label><input type="email" name="email" id="email" required/>
					</p>
					<p class = "formu">
						<label for="mdp">Mot de passe : </label><input type="password" name="password" id="mdp" required minlength="8"/>
						<input type="hidden" name="id" value="{$this->id}"/>
					</p>
					<p class = "formu">
						<button type="submit" name="valider_modification" value="valid_modification">Supprimer</button>
					</p>
				</form>
			</div>
END;
		return $html;
	}
	
}