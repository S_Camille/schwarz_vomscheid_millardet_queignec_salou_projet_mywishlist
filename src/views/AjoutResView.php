<?php

namespace mywishlist\views;

use mywishlist\models\Item;
use mywishlist\models\Liste;
use mywishlist\models\Reservation;

class AjoutResView {

	public function __construct() {}

	public function render() {
		$lisnum = Liste::where('public', '=', 1)->get();
		$html = "";
		foreach ($lisnum as $l) {
    		$it = Item::where('id_reserv', '!=', NULL)->where('liste_id', '=', $l->no)->get();
    		if ($it->count() != 0){
			    $html = $html . "Pour la liste publique n° ".$l->no.", des items sont réservés :</br>";
			 }
			foreach ($it as $item) {
				$html = $html."		- L'Item n°".$item->id.", qui est ".$item->nom." est réservé";
				if (isset($_COOKIE['createur']) && $_COOKIE['createur'] == $l->user_id) {
					$html = $html.".</br>";
				}
				else {
					$res = Reservation::where('id_reserv', '=', $item->id_reserv)->first();
					$html = $html." par ".$res->nom_reserv.".</br>";
				}
			}
		}
		return $html;
	}
}