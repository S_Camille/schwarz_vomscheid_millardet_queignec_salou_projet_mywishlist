<?php

namespace mywishlist\views;

use mywishlist\models\Liste;

class ListeView {

	private $liste;

 	public function __construct(Liste $l) {
 		$this->liste = $l;
	}

 	public function render() {		
		$html = '<div class="content">';
		$html = $html.$this->liste->no." | ".$this->liste->titre." | ".$this->liste->description."<br>";
		$html = $html.'</div>';
		return $html;
	}

	public function render_no() {		
		$html = '<div class="liste_no">';
		$html = $html.$this->liste->no."<br>";
		$html = $html.'</div>';
		return $html;
	}

	public function render_titre() {		
		$html = '<div class="liste_titre">';
		$html = $html.$this->liste->titre."<br>";
		$html = $html.'</div>';
		return $html;
	}

	public function render_desc() {		
		$html = '<div class="liste_desc">';
		$html = $html.$this->liste->description."<br>";
		$html = $html.'</div>';
		return $html;
	}

}