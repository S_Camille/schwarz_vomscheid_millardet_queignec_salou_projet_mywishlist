<?php

namespace mywishlist\views;

class ResImpoView {
	
 	public function __construct() {}

 	public function render() {
 		$app = \Slim\Slim::getInstance();
		$html = GlobaleView::header(['css1' => 'formulaire.css'], 'Se déconnecter');
		$html = $html.<<<END
<p class="error">Vous ne pouvez pas réserver un item d'une liste qui vous appartient.</p>
<p>Redirection vers la page d'accueil dans 5 secondes...<p>
END;
		$html = $html.GlobaleView::footer();
		return $html;
	}
	
}