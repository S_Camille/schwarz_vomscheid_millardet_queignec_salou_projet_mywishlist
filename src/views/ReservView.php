<?php

namespace mywishlist\views;

use mywishlist\models\Item;
use mywishlist\models\Utilisateur;

class ReservView {

	private $id_item;

 	public function __construct(Item $ident) {
 		$this->id_item = $ident;
 	}

	public function render(){
		$app = \Slim\Slim::getInstance();
		$id_item = $this->id_item->id;
		$url = $app->urlFor('AjoutRes').'?no='. $id_item;
		$formulaire = GlobaleView::header(['css1' => 'formulaire.css'], 'Créer une réservation');
		if (isset($_SESSION['user_connected'])) {
			$user=Utilisateur::where('user_id', '=', $_SESSION['user_connected']['user_id'])->first();
			$valNom='value="'.$user->nom.'"';
			$valPrenom='value="'.$user->prenom.'"';
		} else {
			$valNom='';
			$valPrenom='';
		}
		$formulaire = $formulaire.<<<END
		<div id="form">
			<form method="post" action="$url">
				<p class = "formu">
					<label for="nom" class="left">Nom de famille : </label><input type="text" name="nom" id="nom" placeholder="Nom" size="40" maxlength="40" required $valNom/>
				</p>
				<p class = "formu">
					<label for="prenom" class="left">Prénom : </label><input type="text" name="prenom" id="prenom" placeholder="Prenom" size="40" maxlength="40" $valPrenom/>
				</p>
				<p class = "formu">
					<label for="message" class="left">Message destiné à l'utilisateur : </label>
					<textarea name="message" id="message" placeholder="Message"></textarea>
				</p>
				<p class = "formu">
				<input type="hidden" name="id_item" id="id_item" placeholder="Id Item" value="$id_item"/>
				</p>
				<p class = "formu">
					<button type="submit" name="valider_creerliste" value="Valider" />Valider</button>
				</p>
			</form>
		</div>
END;
		$formulaire = $formulaire.GlobaleView::footer();
		return $formulaire;
	}
	
}