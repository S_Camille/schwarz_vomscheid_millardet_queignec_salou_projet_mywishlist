<?php

namespace mywishlist\views;

use mywishlist\Models\Liste;

class ModificationListeView {
	
	private $v;
	private $n;

	public function __construct($v, $n) {
		$this->v=$v;
		$this->n=$n;
	}

	public function aff() {
		$app = \Slim\Slim::getInstance();
		$no = Liste::where("token", "=", $this->n)->first()->no;
		$url1 = $app->urlFor('Titre');
		$url2 = $app->urlFor('Descr');
		$url3 = $app->urlFor('Date');
		$url = $app->urlFor('ItemBouton');
		$html=<<<END
		<div id="form">
		<form method="POST" action="$url1">
			<p class = "formu">
				<label for="titre" class="left">Titre de votre WishList : </label><input type="text" name="titre" size="30" maxlength="30"/>
				<input type="hidden" name="no" value="{$no}"/>
				<input type="hidden" name="token" value="{$this->n}"/>
			</p>
			<p class = "formu">
				<input type="submit" value="Modifier" />
			</p>
		</form>
		<form method="POST" action="$url2">
			<p class = "formu">
				<label for="description" class="left">Description générale de votre WishList : </label>
				<textarea name = "description"></textarea>
				<input type="hidden" name="no" value="{$no}"/>
				<input type="hidden" name="token" value="{$this->n}"/>
			</p>
			<p class = "formu">
				<input type="submit" value="Modifier" />
			</p>
		</form>
		<form method="POST" action="$url3">
			<p class = "formu">
				<label for="date" class="left">Date d'expiration de votre Wishlist : </label><input type="date" name="date" size="30" maxlength="30" />
				<input type="hidden" name="no" value="{$no}"/>
				<input type="hidden" name="token" value="{$this->n}"/>
			</p>
			<p class = "formu">
				<input type="submit" value="Modifier" />
			</p>
		</div>
		</form>
			<form method="POST" action="$url">
			<p class = "formu">
			    <input type="hidden" name="token" value="{$this->n}"/>
				<input type="submit" value="Voir les modifications" />
			</p>
		</form>
END;
		return $html;
	}
	
}