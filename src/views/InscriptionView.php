<?php

namespace mywishlist\views;

class InscriptionView {
	
 	public function __construct() {}

 	public function render() {
 		$app=\Slim\Slim::getInstance();
		$url = $app->urlFor('TryInscription');
		$formulaire = GlobaleView::header(['css1' => 'formulaire.css'], 'S\'inscrire');
		if (isset($_GET['err'])) {
			if (strpos($_GET['err'], '1')!==FALSE)
				$formulaire=$formulaire.'<p class="error">Adresse email déjà existante.</p>';
			if (strpos($_GET['err'], '2')!==FALSE)
				$formulaire=$formulaire.'<p class="error">Pseudo déjà existant.</p>';
			if (strpos($_GET['err'], '3')!==FALSE)
				$formulaire=$formulaire.'<p class="error">Les 2 mots de passe ne sont pas identiques.</p>';
		}
		$formulaire = $formulaire.<<<END
		<div id="form">
		<form method="POST" action="$url" id="connexioninscription">
			<p class = "formu">
				<label for="pseudo" >Pseudo : </label><input type="text" name="pseudo" id="pseudo" placeholder="Pseudo" required />
			</p>

			<p class = "formu">
				<label for="nom" >Nom (facultatif) : </label><input type="text" name="nom" id="nom" placeholder="Nom" />
			</p>

			<p class = "formu">
				<label for="prenom" >Prénom (facultatif) : </label><input type="text" name="prenom" id="prenom" placeholder="Prénom" />
			</p>

			<p class = "formu">
				<label for="email" >Adresse email : </label><input type="email" name="email" id="email" placeholder="Email" required />
			</p>

			<p class = "formu">
				<label for="mdp">Mot de passe : </label><input type="password" name="password" id="mdp" placeholder="Mot de passe" required minlength="8"/>
			</p>

			<p class = "formu">
				<label for="mdp2">Confirmez le mot de passe : </label><input type="password" name="password2" id="mdp2" placeholder="Mot de passe" required minlength="8"/>
			</p>

			<p class = "formu">
				<button type="submit" name="valider_inscription" value="valid_inscription">S'inscrire</button>
			</p>
		</form>
		</div>
END;
		$formulaire = $formulaire.GlobaleView::footer();
		return $formulaire;
	}
	
}