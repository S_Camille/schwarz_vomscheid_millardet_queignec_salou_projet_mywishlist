<?php

namespace mywishlist\views;

class ConfirmerSuppressionView {
	
	public function __construct() {}

 	public function render() {
		$html = GlobaleView::header(['css1' => 'formulaire.css'], 'Suppression du Compte');
		$html = $html.<<<END
<p>Votre compte a bien été supprimé.</p>
<p>Redirection vers la page d'accueil dans 5 secondes...<p>
END;
		$html = $html.GlobaleView::footer();
		return $html;
	}
	
}