<?php

namespace mywishlist\views;

use mywishlist\models\Utilisateur;

class UserView {

	private $id, $rootURI;

 	public function __construct($iden) {
 		$this->id = $iden;
		$app = \Slim\Slim::getInstance();
 		$this->rootURI = $app->request->getRootUri();
 		$this->rootURI = str_replace('index.php','',$this->rootURI);
	}

	public function render() {
		$user = Utilisateur::where('user_id', '=', $this->id)->first();
		$pseudo = $user->pseudo;
		$nom = $user->nom;
		if ($nom == null) {
			$nom = 'Non renseigné';
		}
		$prenom = $user->prenom;
		if ($prenom == null) {
			$prenom = 'Non renseigné';
		}
		$ip = '';
		if (isset($_SESSION['user_connected']) && $_SESSION['user_connected']['auth_level'] > 1) {
			$ip = 'Adresse IP : '.$user->lastIP;
		}
		$app=\Slim\Slim::getInstance();
		$url=$app->urlFor('ListesPubliquesUser', array('id' => $this->id));
		$html=<<<END
		<div id="user">
			<div id="pseudo">$pseudo</div>
			<div id="nom">Nom : $nom</div>
			<div id="prenom">Prénom : $prenom</div>
			<a href="$url">Afficher les listes publiques de $pseudo</a>
		</div> 
END;
		if (isset($_SESSION['user_connected']) && $_SESSION['user_connected']['user_id'] == $this->id) {
			$urlModificationCompte = $app->urlFor('ModificationCompte').'?err=0';
			$html.='<a href="'.$urlModificationCompte.'"><img  id="boutonAdd" src="'.$this->rootURI.'/web/img/modifier.png" alt="Modifier votre compte" title="Modifier votre compte" width="125" height="125" /></a>';
		}
		return $html;
	}

}