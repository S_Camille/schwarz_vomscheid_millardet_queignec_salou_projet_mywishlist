<?php

namespace mywishlist\views;

use mywishlist\models\Item;

class ModificationItemView {
	
	private $item;
	private $id;

	public function __construct($id) {
		$this->item = Item::where("id", "=", $id)->first();
		$this->id = $id;
	}

	public function render() {
    	$html= GlobaleView::header(['css1' => 'formulaire.css'], 'Modification Item');
		$app = \Slim\Slim::getInstance();
        $url = $app->urlFor('SupprimerImageItem') . '?id='.$this->id;
		$url1 = $app->urlFor('EnregistrerModificationItem').'?id='.$this->id;
		$url2 = $app->urlFor('SupprimerImageItem').'?id='.$this->id;
		$url3 = $app->urlFor('DeclencherCagnotte').'?id='.$this->id;
		$url4 = $app->urlFor('Item').'?id='.$this->id;
		$nomItem = $this->item->nom;
		$descrItem = $this->item->descr;
		$urlItem = $this->item->url;
        $tarifItem = $this->item->tarif;
		$html= $html.<<<END
		<script type="text/javascript"> 
        function met(btn,champ1,champ2) { 
            if (btn.checked) { 
                document.getElementById(champ1).style.display="block"; 
                document.getElementById(champ2).style.display="none"; 
                document.getElementById(champ2).value=""; 
            } 
            else { 
                document.getElementById(champ1).style.display="none"; 
                document.getElementById(champ1).value=""; 
                document.getElementById(champ2).style.display="block"; 
            } 
        }
        </script> 
		<div id="form">
		<form method="POST" action="$url1" enctype="multipart/form-data">
			<p class = "formu">
				<label for="nom" class="left">Nom de l'Item : </label>
				<input type="text" name="nomItem" size="30" maxlength="30" value="$nomItem"/>
			</p>
			<p class = "formu">
				<label for="descItem" class="left">Description de l'Item : </label>
				<textarea name = "descItem">$descrItem</textarea>
			</p>
			<input type="radio" name="img" value="fichier" onClick="met(this,'image', 'urlimg')" id="radioButton"/>
			<label for="radioBimage">Importer une image depuis son ordinateur</label>
			<input type="radio" name="img" value="url" onClick="met(this,'urlimg', 'image')" id="radioButton"/>
			<label for="radioBimage">Importer une image depuis une URL</label>
			<p class = "formu" id="image" style="display:none">
				<label for="image" class="left">Image de votre Item : </label>
				<input type="file" name="imgItem" size="30" maxlength="30" />
			</p>
			<p class = "formu" id="urlimg" style="display:none">
				<label for="image" class="left">URL de l'image : </label>
				<input type="url" name="urlImg" maxlength="255" value="$urlItem" />
			</p>
			<p class = "formu" id="url">
				<label for="image" class="left">URL de l'Item : </label>
				<input type="url" name="urlItem" maxlength="255" value="$urlItem" />
			</p>
			<p class = "formu">
				<label for="image" class="left">Tarif de l'Item : </label>
				<input type="number" name="tarifItem" size="30" maxlength="30" step="0.01" min="0" value="$tarifItem"/>
			</p>
			<p class = "formu">
    			<input type="hidden" name="id" value="{$this->id}"/>
				<input type="submit" value="Enregistrer" />
			</p>
		</form>
        </div>
        <div id="form">
        <form method="POST" action="$url2">
			<p class = "formu">
			    <input type="hidden" name="id" value="{$this->id}"/>
				<input type="submit" value="Supprimer l'image de l'Item" />
			</p>
		</form>
        <form method="GET" action="$url4">
			<p class = "formu">
			    <input type="hidden" name="id" value="{$this->id}"/>
				<input type="submit" value="Voir les modifications" />
			</p>
		</form>
END;
        if ($this->item->montCagn == null){
            $url3 = $app->urlFor('DeclencherCagnotte').'?id='.$this->id;
    		$html = $html.<<<END
    		<form method="POST" action="$url3">
    			<p class = "formu">
    			    <input type="hidden" name="id" value="{$this->id}"/>
    				<input type="submit" value="Déclencher la cagnotte" />
    			</p>
    		</form>
    		</div>

END;
        }
        else {
            $url3 = $app->urlFor('AnnulerCagnotte').'?id='.$this->id;
    		$html = $html.<<<END
    		<form method="POST" action="$url3">
    			<p class = "formu">
    			    <input type="hidden" name="id" value="{$this->id}"/>
    				<input type="submit" value="Annuler la cagnotte" />
    			</p>
    		</form>
    		</div>

END;
        }
        $html = $html . GlobaleView::footer();
		return $html;
	}
}