<?php

namespace mywishlist\views;

use mywishlist\models\Item;
use mywishlist\models\Liste;
use mywishlist\models\Cagnotte;

class ItemView {

	private $item, $app, $montantCag, $rootURI;

 	public function __construct(Item $it) {
		$this->app = \Slim\Slim::getInstance();
 		$this->item = $it;
 		$this->montantCag = 0;
 		$cag = Cagnotte::where('item_id', '=', $it);
 		foreach ($cag as $moncag) {
 			$this->montantCag = $this->montantCag + $moncag.montant;
 		}
 		$this->rootURI = $this->app->request->getRootUri();
 		$this->rootURI = str_replace('index.php','',$this->rootURI);
	}

	public function render_id() {		
		$html = '<div class="item_id">';
		$html = $html.$this->item->id." : ";
		$html = $html.'</div>';
		return $html;
	}

	public function render_nom() {		
		$html = '<div class="item_titre">';
		$html = $html.$this->item->nom."<br />";
		$html = $html.'</div>';
		return $html;
	}

	public function render_desc() {		
		$html = '<div class="item_desc">';
		$html = $html.$this->item->descr." : ";
		$html = $html.'</div>';
		return $html;
	}

	public function render_tarif() {		
		$html = '<div class="item_tarif">';
		$html = $html.$this->item->tarif."€<br>";
		$html = $html.'</div>';
		return $html;
	}

	public function render_img() {
		if (filter_var($this->item->img, FILTER_VALIDATE_URL) !== false)
			$img = $this->item->img;
		else 
			$img = $this->rootURI.'/web/img/item/'.$this->item->img;
		$html = '<img src="'.$img.'" alt="'.$this->item->nom.'" height = "150" width = "150"/>';
		return $html;
	}

	public function render_itemres() {
		if ($this->item->id_reserv == null) {
			if (isset($_SESSION['user_connected']) && $_SESSION['user_connected']['user_id'] == Liste::where('no', '=', Item::where('id', '=', $this->item->id)->first()->liste_id)->first()->user_id)
				$url = $this->app->urlFor('ResImpossible');
			else
				$url = $this->app->urlFor('Reserv').'?id='.$this->item->id;
			$html = '<a href="'.$url.'"><img src="'.$this->rootURI.'/web/img/non.png" alt="Item non réservé" title="Item non réservé" height = "150" width = "150"/></a>';
			return $html;
		}
		else {
			$html = '<img src="'.$this->rootURI.'/web/img/oui.png" alt="Item réservé" height = "150" width = "150"/>';
			return $html;
		}
	}

	public function render() {
		$id = $this->item->id;
		$render_itemres = 'render_itemres';
		$nom = $this->item->nom;
		$desc = $this->item->descr;
		$price = $this->item->tarif;
		$cag = $this->montantCag;
		if (filter_var($this->item->img, FILTER_VALIDATE_URL) !== false) {
			$img = $this->item->img;
		}
		else {
			$img = $this->rootURI . '/web/img/item/'.$this->item->img;
		}
		$url1 = $this->app->urlFor('ModifierItem').'?id='.$id;
		$url2 = $this->app->urlFor('SupItem');
		$it = GlobaleView::header(['css1' => 'formulaire.css', 'css2' => 'item.css'], 'Ajout item');
		$l = Liste::where("no","=",$this->item->liste_id)->first();
		if ($this->item->id_reserv == null) {
			$nomResa = "Non réservé";
		}
		else {
			if($this->item->liste->expiration >= date('Y-m-d') && array_key_exists('createur', $_COOKIE)) {
				if($_COOKIE['createur'] == $this->item->liste->user_id){
					$nomResa = "Échéance non atteinte";
				}
				else {
					$nomResa = $this->item->reserv->nom_reserv;
				}
			}
			else {
				$nomResa = $this->item->reserv->nom_reserv;
			}
		}
        $it = $it . <<<END
            <div id="affimg">
		    	<img src="$img" alt="$nom" height = "150" width = "150"/>
            </div>
            <div id="reserv">
            	{$this->render_itemres()}
            </div>
            <div id="detailsitem">
			    <table>
				    <tbody>
					    <tr>
						    <td>Id de l'item</td>
                            <td>$id</td>
                        </tr>
                        <tr>
    						<td>Nom de l'item</td>
                            <td>$nom</td>
                        </tr>
                        <tr>
						    <td>Description de l'item</td>
                            <td>$desc</td>
                        </tr>
                        <tr>
				    	    <td>Tarif de l'item</td>
                            <td>$price</td>
                        </tr>
                        <tr>
                        	<td>Cagnotte</td>
                        	<td>$cag</td>
                        </tr>
                        <tr>
                        	<td>Réservation</td>
                        	<td>$nomResa</td>
                        </tr>
                    </tbody>
                </table>
            </div>
END;
        if (array_key_exists('user_connected', $_SESSION)){
            if ($l->user_id == $_SESSION['user_connected']['user_id']){
                $it = $it . <<<END
                <form method="GET" action="$url1">
                    <p class = "formu">
    				    <button type="submit" name="id" value="$id">Modifier Item</button>
                    </p>
                </form>
                <form method="POST" action="$url2">
                    <p class = "formu">
    				    <button type="submit" name="id" value="$id">Supprimer Item</button>
                    </p>
                </form>
END;
            }
        }
		$it = $it.GlobaleView::footer();
		return $it;
	}

}