<?php

namespace mywishlist\views;

use mywishlist\models\Utilisateur;

class ModifierCompteView {

	private $id;
	
 	public function __construct($id) {
		$this->id=$id;
	}

	public function render() {
		$app=\Slim\Slim::getInstance();
		$url1 = $app->urlFor('NomProfil');
		$url2 = $app->urlFor('PrenomProfil');
		$url3 = $app->urlFor('EmailProfil');
		$url4 = $app->urlFor('PasswordProfil');
		$url5 = $app->urlFor('TrySupression');
		if($_GET["err"]==1){
			$html = '<div"><p class="error">ERREUR LORS DE LA TENTIVE DE SUPPRESSION DE VOTRE COMPTE, EMAIL OU MOT DE PASSE INCORRECT</p></div>';
		}
		else if($_GET["err"]==2){
			$html = '<div><p class="error">ERREUR LORS DE LA TENTIVE DE CHANGEMENT DE MOT DE PASSE, LES 2 MOT DE PASSE SONT DIFFÉRENTS</p></div>';
		}
		else {
			$html = "";
		}
		$html = $html.<<<END
				
		<div id="form">
			<p class = "center">Vous ne pouvez pas modifier votre pseudo.</p>
			<form method="POST" action="$url1">
				<p class = "formu">
					<label for="nom" >Nouveau nom : </label><input type="text" name="nom" id="nom" required/>
					<input type="hidden" name="id" value="{$this->id}"/>
				</p>
				<p class = "formu">
					<button type="submit" name="valider_modification" value="valid_modification">Modifier</button>
				</p>
			</form>
			<form method="POST" action="$url2">
				<p class = "formu">
					<label for="prenom" >Nouveau prénom : </label><input type="text" name="prenom" id="prenom" required />
					<input type="hidden" name="id" value="{$this->id}"/>
				</p>
				<p class = "formu">
					<button type="submit" name="valider_modification" value="valid_modification">Modifier</button>
				</p>
			</form>
			<form method="POST" action="$url3">
				<p class = "formu">
					<label for="email" >Nouvelle adresse email : </label><input type="email" name="email" id="email" required />
					<input type="hidden" name="id" value="{$this->id}"/>
				</p>
				<p class = "formu">
					<button type="submit" name="valider_modification" value="valid_modification">Modifier</button>
				</p>
			</form>
			<form method="POST" action="$url4">
				<p class = "formu">
					<label for="mdp">Nouveau mot de passe : </label><input type="password" name="password" id="mdp" required minlength="8"/>
				</p>
				<p class = "formu">
					<label for="mdp2">Confirmez le nouveau mot de passe : </label><input type="password" name="password2" id="mdp2" required minlength="8"/>
					<input type="hidden" name="id" value="{$this->id}"/>
				</p>
				<p class = "formu">
					<button type="submit" name="valider_modification" value="valid_modification">Modifier</button>
				</p>
			</form>
			<form method="POST" action="$url5">
				<p class = "formu">
					<button type="submit" name="valider_modification" value="valid_modification">Supprimer Votre compte</button>
					<input type="hidden" name="id" value="{$this->id}"/>
				</p>
			</form>
		</div>
END;
		$html = $html.GlobaleView::footer();
		return $html;
	}

}