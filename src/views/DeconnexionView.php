<?php

namespace mywishlist\views;

class DeconnexionView {
	
 	public function __construct() {}

 	public function render() {
 		$app = \Slim\Slim::getInstance();
		$html = GlobaleView::header(['css1' => 'formulaire.css'], 'Se déconnecter');
		$html = $html.<<<END
<p>Vous avez bien été déconnecté.</p>
<p>Redirection vers la page d'accueil dans 5 secondes...<p>
END;
		$html = $html.GlobaleView::footer();
		return $html;
	}
	
}