<?php

namespace mywishlist\views;

class CreateListView {
	
 	public function __construct() {}

	public function render (){
		$app = \Slim\Slim::getInstance();
		$url = $app->urlFor('AjoutListe');
		$formulaire = GlobaleView::header(['css1' => 'formulaire.css'], 'Créer une liste');
		$formulaire = $formulaire . <<<END
		<div id="form">
		<form method="post" action="$url">
			<p class = "formu">
				<label for="titre" class="left">Titre de votre WishList : </label><input type="text" name="titre" id="titre" placeholder="Titre" size="40" maxlength="40" required/>
			</p>

			<p class = "formu">
				<label for="description" class="left">Description générale de votre WishList : </label>
				<textarea name="description" id="description" placeholder="Description" required ></textarea>
			</p>

			<p class = "formu">
				<label for="date" class="left">Date d'expiration de votre Wishlist : </label><input type="date" name="datetime-local" size="30" maxlength="30" required />
			</p>

			<p class = "formu">
				<button type="submit" name="valider_creerliste" value="Valider" />Valider</button>
			</p>
		</form>
		</div>
END;
		$formulaire = $formulaire.GlobaleView::footer();
		return $formulaire;
	}

}