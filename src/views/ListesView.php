<?php

namespace mywishlist\views;

class ListesView {

	private $listes, $rootURI;

 	public function __construct($l) {
 		$this->listes = $l;
		$app = \Slim\Slim::getInstance();
		$this->rootURI = $app->request->getRootUri();
 		$this->rootURI = str_replace('index.php','',$this->rootURI);
	}

 	public function render() {
 		$app = \Slim\Slim::getInstance();	
		$html = '<div id="titres">';
		$i = 0;
		foreach ($this->listes as $l)  {
			$html = $html.'<div class="';
			if ($i%2==0) {
				$html = $html.'pair';
			}
			else {
				$html = $html.'impair';
			}
			$titre = '';
			if (strlen($l->titre) > 40) {
				$titre = substr($l->titre, 0, 40).'...';
			}
			else {
				$titre = $l->titre;
			}
			$html = $html.'">'.$titre.'</div>';
			$i++;
		}
		$html = $html.'</div>';
		$html = $html.'<div id="descrs">';
		$i = 0;
		foreach ($this->listes as $l)  {
			$html = $html.'<div class="';
			if ($i%2==0) {
				$html = $html.'pair';
			}
			else {
				$html = $html.'impair';
			}
			$desc = '';
			if (strlen($l->description)>85) {
				$desc = substr($l->description, 0, 85).'...';
			}
			else {
				$desc = $l->description;
			}
			$html = $html.'">'.$desc.'</div>';
			$i++;
		}
		$html = $html.'</div>';
		$html = $html.'<div id="afficher">';
		$i = 0;
		foreach ($this->listes as $l)  {
			$html = $html.'<div class="';
			if ($i%2==0) {
				$html = $html.'pair';
			}
			else {
				$html = $html.'impair';
			}
			$url = $app->urlFor('Liste').'?no='.$l->token;
			$html = $html.'"><a href="'.$url.'" title="Afficher la liste avec les items">Afficher</a></div>';
			$i++;
		}
		$html = $html.'</div>';
		$html = $html.'<div id="modif">';
		$i = 0;
		foreach ($this->listes as $l)  {
			$html = $html.'<div class="';
			if ($i%2==0) {
				$html = $html.'pair';
			}
			else {
				$html = $html.'impair';
			}
			$url = $app->urlFor('Modification').'?no='.$l->token;
			$html = $html.'"><a href="'.$url.'" title="Modifier la liste (titre, description)">Modifier</a></div>';
			$i++;
		}
		$html = $html.'</div>';
		$html = $html.'<div id="consulteRes">';
		$i = 0;
		foreach ($this->listes as $l) {
			$html = $html.'<div class="';
			if ($i%2==0) {
				$html = $html.'pair';
			}
			else {
				$html = $html.'impair';
			}
			$url=$app->urlFor('ConsulterResPriv').'?no='.$l->token;
			$html=$html.'"><a href="'.$url.'" title="Consulter les réservations">Consulter les réservations</a></div>';
			$i++;
		}
		$html = $html.'</div>';
		$html = $html.'<div id="public">';
		$i = 0;
		foreach ($this->listes as $l) {
			$html = $html.'<div class="';
			if ($i%2==0) {
				$html = $html.'pair';
			}
			else {
				$html = $html.'impair';
			}
			$url = $app->urlFor('RendrePublic');
			$rootURI = $this->rootURI;
			if($l->public != 0){
			    $html=$html.<<<END
			    ">
			    <form method="POST" action="$url" enctype="multipart/form-data">
			        <label> Publique : </label>
			        <input type="image" src="$rootURI/web/img/publicValide.png" alt="Rendre la liste non publique" title="Rendre la liste non publique" width="13" height="13"/>
			        <input type="hidden" name="token" value="{$l->token}"/>
			     </form>
			     </div>
END;
			}
			else {
    			$html=$html. <<<END
			    ">
			    <form method="POST" action="$url" enctype="multipart/form-data">
			        <label> Publique : </label>
			        <input type="image" src="$rootURI/web/img/publicNonValide.png" alt="Rendre la liste publique" title="Rendre la liste publique" width="13" height="13"/>
			        <input type="hidden" name="token" value="{$l->token}"/>
			     </form>
			     </div>
END;
			}
			$i++;
		}
		$html = $html.'</div>';
		$html = $html.'<div id="suppr">';
		$i = 0;
		foreach ($this->listes as $l) {
			$html = $html.'<div class="';
			if ($i%2==0) {
				$html = $html.'pair';
			}
			else {
				$html = $html.'impair';
			}
			$url=$app->urlFor('SuppressionConfirme').'?no='.$l->no;
			$html=$html.'"><a href="'.$url.'"><img src="'.$rootURI.'/web/img/supprimer.png" alt="Supprimer la liste" title="Supprimer la liste" width="13" height="13" /></a></div>';
			$i++;
		}
		$html = $html.'</div>';
		return $html;
	}

}