<?php

namespace mywishlist\views;

use mywishlist\models\Liste;

class AjoutItemView {
	
 	public function __construct() {}

 	public function render() {
 		$app = \Slim\Slim::getInstance();
     	$url = $app->urlFor('AjoutElement');
     	$token = $app->request->get('no');
     	$liste = Liste::where("token", "=", $token)->first();
     	$no = $liste->no;
		$html = <<<END
		<script type="text/javascript"> 
function met(btn,champ1,champ2) { 
	if (btn.checked) { 
		document.getElementById(champ1).style.display="inline"; 
		document.getElementById(champ2).style.display="none"; 
		document.getElementById(champ2).value=""; 
	} 
 	else { 
    	document.getElementById(champ1).style.display="none"; 
    	document.getElementById(champ1).value=""; 
		document.getElementById(champ2).style.display="display"; 
   } 
}
</script> 
<form method="POST" action="$url" id="ajoutItem" enctype="multipart/form-data">
	<label>Nouvel Item </label>
	<input type="text" name="nomItem" id="nomItem" placeholder="Nom de l'Item" required />
	<input type="text" name="descrItem" id="descrItem" placeholder="Description de l'Item" required />
	<input type="radio" name="img" value="fichier" onClick="met(this,'imgItem', 'urlImg')" />
	<label>Image depuis un fichier</label>
	<input type="file" name="imgItem" id="imgItem" accept="image/*" style="display:none" />
	<input type="hidden" name="MAX_FILE_SIZE" value="30000" />
    <input type="radio" name="img" value="URL" onClick="met(this, 'urlImg', 'imgItem')"/>
    <label>Image depuis une URL</label>
	<input type="url" name="urlImg" id="urlImg" placeholder="URL de l'Image" style="display:none"/>
    <input type="url" name="urlItem" id="urlItem" placeholder="URL de l'Item"/>
	<input type="number" name="tarifItem" id="tarifItem" placeholder="Tarif de l'Item" step="0.01" min="0"/>
	<input type="hidden" name="token" id="token" value="$token"/>
	<input type="hidden" name="no" id="no" value="$no"/>
	<button type="submit" name="ajoute_item" value="ajout_item">Ajouter élément</button>
</form>
END;
		return $html;
	}
	
}