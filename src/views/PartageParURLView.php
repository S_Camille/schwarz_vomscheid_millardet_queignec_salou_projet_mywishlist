<?php

namespace mywishlist\views;

class PartageParURLView {
	
	public function __construct($t) {
		$this->token=$t;
	}

	public function bouton(){
		$app = \Slim\Slim::getInstance();
		$url= $_SERVER['HTTP_HOST'] . $app->urlFor('Liste')."?no=".$this->token;
		$html=<<<END
		<p class = "formu">
			<button class="js-textareacopybtn" style="vertical-align:top;">Copier l'URL</button>
  			<textarea class="js-copytextarea">$url</textarea>
			<script type="text/javascript">
				var copyTextareaBtn = document.querySelector('.js-textareacopybtn');
				copyTextareaBtn.addEventListener('click', function(event) {
  					var copyTextarea = document.querySelector('.js-copytextarea');
  					copyTextarea.select();
  					try {
    					var successful = document.execCommand('copy');
    					var msg = successful ? 'successful' : 'unsuccessful';
    					console.log('Copying text command was ' + msg);
  					} catch (err) {
    					console.log('Oops, unable to copy');
  					}
				});
			</script>
			
		</p>
END;
		return $html;
	}
	
}