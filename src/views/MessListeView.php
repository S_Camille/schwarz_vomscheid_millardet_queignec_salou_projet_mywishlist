<?php

namespace mywishlist\views;

use mywishlist\models\Liste;
use mywishlist\models\Utilisateur;
use mywishlist\models\MessageListe;

class MessListeView {

 	public static function render() {
		$app = \Slim\Slim::getInstance();
     	$messages = MessageListe::where('liste_id', '=', Liste::where('token', '=', $_GET['no'])->first()->no)->get();
		$html = '<div id="commentaires">';
		foreach ($messages as $message) {
			$pseudo = Utilisateur::where('user_id', '=', $message->user_id)->first()->pseudo;
			$com = $message->message;
			$url = $app->urlFor('User', array('id'=>$message->user_id));
			$html = $html.<<<END
<div class="com"><div class="auteur">De : <a href="$url">$pseudo</a></div><div class="message">$com</div></div>
END;
		}
		$html = $html.'</div>';
		return $html;
	}

}