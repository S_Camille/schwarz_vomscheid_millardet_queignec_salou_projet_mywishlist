<?php

namespace mywishlist\views;

class SuppressionListeView {

	private $liste;

 	public function __construct($l) {
 		$this->liste=$l;
	}

 	public function render() {
 		$app=\Slim\Slim::getInstance();	
		$html = '<p id="supprListe">Voulez-vous vraiment supprimer la liste <b>'.$this->liste->titre.'</b> ?</p>';
		$urlSuppr = $app->urlFor('SuppressionListe');
		$urlMesListes = $app->urlFor('ListeUser');
		$no = $this->liste->no;
		$html = $html.<<<END
		<div id="reponseSupprListe">
			<form method="POST" action="$urlSuppr" id="supprListeOui">
				<input type="hidden" name="nolistesuppr" value="$no"/>
				<button type="submit" name="valider_supprListe" value="supprListe">Oui</button>
			</form>
			<form method="GET" action="$urlMesListes" id="supprListeNon">
				<input type="submit" value="Non"/>
			</form>
		</div>
END;
		return $html;
	}
	
}