<?php

namespace mywishlist\controllers;

use mywishlist\models\Item;
use mywishlist\models\Liste;
use mywishlist\views\GlobaleView;
use mywishlist\views\CreateListView;
use mywishlist\views\ReservView;

class ReservController {

	private $ide;

	public function __construct($ite) {
		$this->ide = Item::where('id', '=', $ite)->first();
	}

	public function afficheReservation(){
		$app=\Slim\Slim::getInstance();
		if (isset($_SESSION['user_connected']) && $_SESSION['user_connected']['user_id'] == Liste::where('no', '=', $this->ide->liste_id)->first()->user_id)
				$app->redirect($app->urlFor('ResImpossible'));
		$rv = new ReservView($this->ide);
		echo $rv->render();
	}

}