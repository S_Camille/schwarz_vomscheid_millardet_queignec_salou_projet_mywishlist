<?php

namespace mywishlist\controllers;

use mywishlist\views\SuppressionCompteView;
use mywishlist\models\Utilisateur;
use mywishlist\models\Liste;
use mywishlist\models\Item;
use mywishlist\models\MessageListe;
use mywishlist\views\GlobaleView;

class SuppressionCompteController {
	
	private $id;

	public function __construct($ident) {
		$this->id = $ident;
	}

	public function affiche(){
		$head = GlobaleView::header(['css1' => 'formulaire.css'], 'Supprimer Compte');
		$foot = GlobaleView::footer();
		$scv = new SuppressionCompteView($this->id);
		echo $head.$scv->render().$foot;	
	}
	
	public function supprimer() {
		if (isset($_SESSION['user_connected'])) {
			session_destroy();
		}
		$utilisateur = Utilisateur::where('user_id', '=', $this->id)->first();
		$listes = Liste::where('user_id', '=', $this->id)->get();
		foreach ($listes as $liste) {
			$items = Item::where('liste_id', '=', $liste->no)->get();
			foreach ($items as $item) {
				$item->delete();
			}
			$mess = MessageListe::where('liste_id', '=', $liste->no)->get();
			foreach ($mess as $mes) {
				$mes->delete();
			}
			$liste->delete();
		}
		$messages = MessageListe::where('user_id', '=', $this->id)->get();
		foreach ($messages as $message) {
			$message->delete();
		}
		$utilisateur->delete();
	}

	public function initdelete() {
		$app = \Slim\Slim::getInstance();
		$utilisateur = Utilisateur::where('user_id', '=', $app->request->post('id'))->first();
		$pass = $app->request->post('password');
		if (password_verify($pass, $utilisateur->password) && $utilisateur->email == $app->request->post('email')) {
			$this->supprimer();
			$app->redirect($app->urlFor('ConfirmerSup'));
		}
		else {
			$app->redirect($app->urlFor('ModificationCompte')."?err=1");
		}
	}

}