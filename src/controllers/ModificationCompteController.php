<?php

namespace mywishlist\controllers;

use mywishlist\views\ModifierCompteView;
use mywishlist\models\Utilisateur;
use mywishlist\views\GlobaleView;

class ModificationCompteController {

	private $profil;
	private $html;

	public function __construct($id) {
		if (is_null($id)) {
			$this->html = GlobaleView::header(['css1' => 'formulaire.css'], 'S\'inscrire');
			$this->profil = $_SESSION['user_connected'];
		}
		else {
			$this->profil = Utilisateur::where("user_id", "=", $id)->first();
		}
	}

	public function affichePageModification () {
		$aff = new ModifierCompteView($this->profil["user_id"]);
		echo $this->html.$aff->render();
	}

	public function ModifierNom($nom){
		$app = \Slim\Slim::getInstance();
		$this->profil->nom = filter_var($nom, FILTER_SANITIZE_SPECIAL_CHARS);
		$this->profil->save();
		$app->redirect($app->urlFor('ListeUser'));
	}

	public function ModifierPrenom($prenom){
		$app = \Slim\Slim::getInstance();
		$this->profil->prenom = filter_var($prenom, FILTER_SANITIZE_SPECIAL_CHARS);
		$this->profil->save();
		$app->redirect($app->urlFor('ListeUser'));
	}

	public function ModifierPassword($password){
		$this->profil->password = password_hash($password, PASSWORD_DEFAULT);
		$this->profil->save();
	}

	public function ModifierEmail($email){
		$app = \Slim\Slim::getInstance();
		$this->profil->email = filter_var($email, FILTER_SANITIZE_SPECIAL_CHARS);
		$this->profil->save();
		$app->redirect($app->urlFor('ListeUser'));
	}

	public function initmodmdp() {
		$app = \Slim\Slim::getInstance();
		if ($app->request->post('password') == $app->request->post('password2')) {
			$this->ModifierPassword($app->request->post('password'));
			$app->redirect($app->urlFor('ListeUser'));
		}
		else {
			$app->redirect($app->urlFor('ModificationCompte')."?err=2");
		}
	}

}