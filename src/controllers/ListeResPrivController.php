<?php

namespace mywishlist\controllers;

use mywishlist\views\GlobaleView;
use mywishlist\views\AjoutResPrivView;

class ListeResPrivController {
    
    private $token;

	public function __construct($token) {
    	$this->token = $token;
	}

	public function afficListeRes() {
		$app = \Slim\Slim::getInstance();
		$head = GlobaleView::header([], 'Liste des Réservations');
		$foot = GlobaleView::footer();
		$arv = new AjoutResPrivView($this->token);
		echo $head.$arv->render().$foot;
	}

}