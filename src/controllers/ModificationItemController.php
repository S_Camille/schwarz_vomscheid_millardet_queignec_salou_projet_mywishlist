<?php

namespace mywishlist\controllers;

use mywishlist\views\GlobaleView;
use mywishlist\views\ModificationItemView;
use mywishlist\models\Item;

class ModificationItemController {
    
    private $id;
    
    function __construct($ident) {
        $this->id = $ident;
    }

	public function affiche() {
		$aff = new ModificationItemView($this->id);
		echo $aff->render();
	}
	
	public static function modifierItem($no, $nom, $desc, $img, $url, $tarif){
    	$i = Item::where("id","=",$no)->first();
    	$i->nom = $nom;
    	$i->descr = $desc;
    	$i->img = $img;
    	$i->url = $url;
    	$i->tarif = $tarif;
    	$i->save();
    	return $i->id;
	}

}