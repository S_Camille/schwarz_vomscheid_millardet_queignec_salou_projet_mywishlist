<?php

namespace mywishlist\controllers;

use mywishlist\views\InscriptionView;
use mywishlist\models\Utilisateur;

class InscriptionController {

	public function __construct() {}

	public function inscrire() {
		$app = \Slim\Slim::getInstance();
		$request = $app->request;
		$bouton = $request->post('valider_inscription');
		if (isset($bouton) && $bouton == 'valid_inscription') {
			$ger = '';
			if (!Utilisateur::where('email', '=', $request->post('email'))->get()->isEmpty()) {
				$ger = $ger.'1';
			}
			if (!Utilisateur::where('pseudo', '=', $request->post('pseudo'))->get()->isEmpty()) {
				$ger = $ger.'2';
			}
			if ($request->post('password') != $request->post('password2')) {
				$ger = $ger.'3';
			}
			if ($ger != '') {
				$app->redirect($app->urlFor('Inscription').'?err='.$ger);
			}
			Authentification::createUser(filter_var($request->post('pseudo'), FILTER_SANITIZE_SPECIAL_CHARS), $request->post('email'), $request->post('password'), $request->post('nom'), $request->post('prenom'),
										 'email', 'pseudo', 'password', 'nom', 'prenom');
			$app->redirect($app->urlFor('Connexion'));
		}
		
	}
	
	public function affichePageInscr() {
		$aff = new InscriptionView();
		$app = \Slim\Slim::getInstance();
		if (isset($_SESSION['user_connected'])) {
			$app->redirect($app->urlFor('Acceuil'));
		}
		echo $aff->render();
	}

}