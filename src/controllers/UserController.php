<?php

namespace mywishlist\controllers;

use mywishlist\views\UserView;
use mywishlist\models\Utilisateur;
use mywishlist\views\GlobaleView;

class UserController {

	private $id;

	public function __construct($id) {
		$app = \Slim\Slim::getInstance();
		if (Utilisateur::where('user_id', '=', $id)->get()->isEmpty()) {
			$app->redirect($app->urlFor('Accueil'));
		}
		$this->id = $id;
	}

	public function afficher(){
		echo GlobaleView::header([], 'Utilisateur');
		$uv=new UserView($this->id);
		echo $uv->render();
		echo GlobaleView::footer();	
	}
	
}