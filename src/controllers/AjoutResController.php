<?php

namespace mywishlist\controllers;

use mywishlist\views\GlobaleView;
use mywishlist\views\CreateListView;
use mywishlist\views\ReservView;
use mywishlist\models\Reservation;
use mywishlist\models\Item;

class AjoutResController {

	public function __construct() {}

	public function ajoutReserv() {
		$app = \Slim\Slim::getInstance();
		$res = new Reservation();
		$res->nom_reserv = $app->request->post('nom').' '.$app->request->post('prenom');
		$_SESSION['pseudo'] = $app->request->post('nom');
		$res->mess_reserv = $app->request->post('message');
		$res->save();
		$item = Item::where('id', '=', $app->request->post('id_item'))->first();
		$item->id_reserv = $res->id_reserv;
		$item->save();
		$app->redirect($app->urlFor('Item').'?id='.$item->id);
	}

}