<?php

namespace mywishlist\controllers;

use mywishlist\views\SuppressionListeView;
use mywishlist\models\Liste;
use mywishlist\views\GlobaleView;

class SuppressionListeController {
	
	private $no;

	public function __construct($n) {
		$this->no = $n;
	}

	public function affiche() {
		$app = \Slim\Slim::getInstance();
		echo GlobaleView::header([], 'Supprimer Liste');
		$liste = Liste::where('no' ,'=', $this->no)->where('user_id' ,'=', $_SESSION['user_connected']['user_id'])->first();
		if ($liste == NULL) {
			$app->redirect($app->urlFor('Accueil'));
		}
		$view = new SuppressionListeView($liste);
		echo $view->render().GlobaleView::footer();	
	}
	
	public function supprimer() {
		$liste = Liste::where('no' ,'=', $this->no)->where('user_id' ,'=', $_SESSION['user_connected']['user_id'])->first();
		if ($liste != NULL) {
			$liste->delete();		
		}
	}

	public function initsuplis() {
		$app = \Slim\Slim::getInstance();
		if (isset($_SESSION['user_connected'])) {
			$bouton = $app->request->post('valider_supprListe');
			if (isset($bouton)) {
				$this->supprimer();
				$app->redirect($app->urlFor('ListeUser'));
			}
		}
		$app->redirect($app->urlFor('Accueil'));
	}

}