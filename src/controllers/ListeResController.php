<?php

namespace mywishlist\controllers;

use mywishlist\views\GlobaleView;
use mywishlist\views\AjoutResView;

class ListeResController {

	public function __construct() {}

	public function afficListeRes() {
		$app = \Slim\Slim::getInstance();
		$head = GlobaleView::header([], 'Liste des Réservations');
		$foot = GlobaleView::footer();
		$arv = new AjoutResView();
		echo $head.$arv->render().$foot;
	}

}