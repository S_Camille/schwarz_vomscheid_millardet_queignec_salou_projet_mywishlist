<?php

namespace mywishlist\controllers;

use mywishlist\views\ConfirmerSuppressionView;

class ConfirmerSuppressionController {

	public function __construct() {}

	public function afficher() {
		$aff = new ConfirmerSuppressionView();
		header('Refresh: 5; URL='.\Slim\Slim::getInstance()->urlFor('Accueil'));
		echo $aff->render();
	}

}