<?php

namespace mywishlist\controllers;

use mywishlist\views\GlobaleView;

class AccueilController {
	
	public function __construct() {}

	public function affichageAcc() {
		$aff = new AfficheListeController();
		$aff->affichageListes();
	}

}