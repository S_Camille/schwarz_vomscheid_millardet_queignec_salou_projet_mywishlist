<?php

namespace mywishlist\controllers;

use mywishlist\views\ResImpoView;

class ResImpoController {	
	public static function affiche() {
		$app = \Slim\Slim::getInstance();
		$aff = new ResImpoView();
		if (!isset($_SESSION['user_connected'])) {
			$app->redirect($app->urlFor('Accueil'));
		}
		header('Refresh: 5; URL='.$app->urlFor('Accueil'));
		echo $aff->render();
	}

}