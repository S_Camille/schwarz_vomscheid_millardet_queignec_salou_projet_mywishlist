<?php

namespace mywishlist\controllers;

use mywishlist\views\ModificationListeView;
use mywishlist\models\Liste;
use mywishlist\views\GlobaleView;

class ModificationListeController {
	
	private $n;
	private $liste;

	public function __construct($no) {
		$this->n = $no;
		$this->liste = Liste::where('no' ,'=', $no)->first();
	}

	public function affiche(){
		$head = GlobaleView::header([], 'Afficher Liste');
		$foot = GlobaleView::footer();
		$mv = new ModificationListeView($this->liste, $this->n);
		echo $head.$mv->aff().$foot;
	}

	public function ModifierTitre($t){
		$app = \Slim\Slim::getInstance();
		$this->liste->titre = $t;
		$this->liste->save();
		$app->redirect($app->urlFor('Modification').'?no='.$app->request->post('token'));
	}

	public function ModifierDescr($d){
		$app = \Slim\Slim::getInstance();
		$this->liste->description = $d;
		$this->liste->save();
		$app->redirect($app->urlFor('Modification').'?no='.$app->request->post('token'));
	}

	public function ModifierDate($da){
		$app = \Slim\Slim::getInstance();
		$this->liste->expiration = $da;
		$this->liste->save();
		$app->redirect($app->urlFor('Modification').'?no='.$app->request->post('token'));
	}

}