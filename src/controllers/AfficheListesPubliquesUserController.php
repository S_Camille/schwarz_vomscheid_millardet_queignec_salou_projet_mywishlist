<?php

namespace mywishlist\controllers;

use mywishlist\models\Liste;
use mywishlist\models\Utilisateur;
use mywishlist\views\ListeView;
use mywishlist\views\GlobaleView;

class AfficheListesPubliquesUserController {
	private $id;
	public function __construct($id) {
		$app = \Slim\Slim::getInstance();
		if (Utilisateur::where('user_id', '=', $id)->get()->isEmpty()) {
			$app->redirect($app->urlFor('Accueil'));
		}
		$this->id=$id;
	}

	public function affichageListes() {
		$head = GlobaleView::header([], 'afficher listes publiques');
		$foot = GlobaleView::footer();
		$html = $head;
		$al = new AfficheListeController();
		$listes = Liste::where('public', '=',1)->where('user_id', '=', $this->id)->get();
		foreach ($listes as $l) {
		    $html = $html.$al->afficheListe($l)."<br />";
        }
		$html = $html.$foot;
		echo $html;
	}

}