<?php

namespace mywishlist\controllers;

use mywishlist\models\Item;
use mywishlist\models\Cagnotte;
use mywishlist\views\ListesView;
use mywishlist\views\GlobaleView;

class CagnotteController {
    
	private $item;
	private $cagnotte;
	
	public function __construct($id) {
		$this->item = Item::where('id', '=', $id)->first();
		$this->cagnotte = Cagnotte::where('item_id', '=', $id)->get();
	}

	public function affichageCagnotte(){
		$html = "";
        $app = \Slim\Slim::getInstance();
		if ($this->item->montCagn != null) {
    		$cagnotte = 0;
    		foreach ($this->cagnotte as $l) {
        		$cagnotte = $cagnotte + $l->montant;
    		}
    		$max = $this->item->montCagn - $cagnotte;
    		$id = $this->item->id;
    		if(array_key_exists("user_connected",$_SESSION)) {
        		if($_SESSION['user_connected']['user_id'] != $this->item->user_id) {
        		    $url = $app->urlFor('ParticiperCagnotte');
        		    $html = <<<END
            		<form method="POST" action="$url" enctype="multipart/form-data">
            		    <input type="number" name="montPart" id="Part" placeholder="Montant de la participation à la cagnotte" step="0.1" min="0" max="$max"/>
            		    <input type='hidden' name="id" value="$id" />
            		    <input type="submit" value="Je participe !" />
            		</form>
END;
        		}
        		else {
            		if ($this->cagnotte->first() == null){}
        		    else {
            		    $html = '<label> Actuellement, $cagnotte € ont été récupérés sur $max €</label>';
                    }
    		    }
    		}
        }
        else {
            $html = "Pas de cagnotte en cours";
        }
        return $html;
	}

    public function initPartCagnotte() {
        $app = \Slim\Slim::getInstance();
        if (isset($_SESSION['user_connected'])) {
            $acg = new Cagnotte();
            $acg->user_id = $_SESSION['user_connected']['user_id'];
            $acg->item_id = $app->request->post('id');
            $acg->montant = $app->request->post('montPart');
            $acg->message = "";
            $acg->save();
        }
        $app->redirect($app->urlFor('Item').'?id='.$app->request->post('id'));
    }

    public function initModCagnotte() {
        $app = \Slim\Slim::getInstance();
        if (isset($_SESSION['user_connected'])) {
            $acg = Cagnotte::where('id','=',$app->request->post('id'))->where('user_id','=',$_SESSION['user_connected']['user_id'])->first();
            if ($app->request->post('montPart') >= 0) {
                $acg->montant = $app->request->post('montPart');
            }
            $acg->save();
        }
        $app->redirect($app->urlFor('Item').'?id='.$app->request->post('id'));
    }

    public function initDecCagnotte() {
        $app = \Slim\Slim::getInstance();
        if (isset($_SESSION['user_connected'])) {
            $item = Item::where('id', '=', $app->request->post('id'))->first();
            $item->montCagn = $item->tarif;
            $item->save();
        }
        $app->redirect($app->urlFor('ModifierItem').'?id='.$app->request->post('id'));
    }

    public function initAnnCagnotte() {
        $app = \Slim\Slim::getInstance();
        if (isset($_SESSION['user_connected'])) {
            $item = Item::where('id', '=', $app->request->post('id'))->first();
            $item->montCagn = null;
            $item->save();
        }
        $app->redirect($app->urlFor('ModifierItem').'?id='.$app->request->post('id'));
    }

}