<?php

namespace mywishlist\controllers;

use mywishlist\views\GlobaleView;
use mywishlist\models\Item;
use mywishlist\models\Liste;
use mywishlist\views\ItemView;

class ItemController {

	private $id;

	public function __construct($n) {
		$this->id = $n;
	}

	public function affichageItem() {
		$item = Item::where('id', '=', $this->id)->first();
		$vi = new ItemView($item);
		echo $vi->render();
	}

	public static function supItem($id) {
		$app = \Slim\Slim::getInstance();
		$item = Item::where('id', '=', $id)->first();
		$token = Liste::where('no', '=', $item->liste_id)->first()->token;
		$item->delete();
		$app->redirect($app->urlFor('Liste').'?no='.$token);
		return $token;
	}
	
	public function supImgItem() {
		$app = \Slim\Slim::getInstance();
	    $item = Item::where('id', '=', $app->request->post('id'))->first();
		if (filter_var($item->img, FILTER_VALIDATE_URL) === false)
			unlink('web/img/item/'.$item->img);
	    $item->img = null;
	    $item->save();
	    $app->redirect($app->urlFor('Item').'?id='.$app->request->post('id'));
	}

	public function initModItem() {
		$app = \Slim\Slim::getInstance();
	    $id = $app->request->post('id');
	    $i = Item::where("id", "=", $id)->first();
		if ($app->request->post('urlImg')!='') {
			if (filter_var($i->img, FILTER_VALIDATE_URL) === false)
				unlink('web/img/item/'.$i->img);
			 $img = $app->request->post('urlImg');
		}
		else if (strlen($_FILES["imgItem"]["name"]) != 0) {
			if (filter_var($i->img, FILTER_VALIDATE_URL) === false)
				unlink('web/img/item/'.$i->img);
	        $img = $_FILES["imgItem"]["name"];
		}
	    else 
			$img=$i->img;
	    $id = ModificationItemController::modifierItem($id, $app->request->post('nomItem'), $app->request->post('descItem'), $img , $app->request->post('urlItem') , $app->request->post('tarifItem'));
	    if ($_FILES["imgItem"]["name"] != null) {
	        $res = move_uploaded_file($_FILES["imgItem"]["tmp_name"],"web/img/item/".$_FILES["imgItem"]["name"]);
	    }
	    if ($id > -1) {
	        $app->redirect($app->urlFor('Item').'?id='.$id);
	    }
	    else {
	        $app->redirect($app->urlFor('Accueil'));
	    }
	}

}