<?php

namespace mywishlist\controllers;

use mywishlist\models\Utilisateur;
use mywishlist\models\Role;

class Authentification {

	public function __construct() {}

	public static function authentification($nomBoutonCo, $valBoutonCo, $nomFormuMail, $nomFormuPassword, $nomBDDmail, $nomBDDpassword, $nomBDDUserID='0') {
		$app = \Slim\Slim::getInstance();
		$request = $app->request;
		$bouton = $request->post($nomBoutonCo);
		if (isset($bouton) && $bouton == $valBoutonCo &&  filter_var($request->post($nomFormuMail), FILTER_VALIDATE_EMAIL) !== false) {
			$user = Utilisateur::where($nomBDDmail, '=', $request->post($nomFormuMail))->first();
			if ($user != NULL && password_verify($request->post($nomFormuPassword), $user->$nomBDDpassword)) {
				$_SESSION['user_connected']=array('email' => $user->$nomBDDmail,
												  'user_id' => $user->$nomBDDUserID,
												  'ip_client' => $_SERVER['REMOTE_ADDR'],
												  'auth_level' => Role::where('role_id', '=', $user->role_id)->first()->auth_level);
				setcookie('createur', $_SESSION['user_connected']['user_id'], time() + 3600*325*24, "/", null, false, true);
				return $user;
			}
		}
		return false;
	}
	
	public static function createUser($pseudo, $mail, $password, $nom, $prenom, $nomBDDmail, $nomBDDpseudo, $nomBDDpassword, $nomBDDnom, $nomBDDprenom) {
		$user = new Utilisateur();
		$user->$nomBDDmail = $mail;
		$user->$nomBDDpseudo = $pseudo;
		if ($nom != '')
			$user->$nomBDDnom = filter_var($nom, FILTER_SANITIZE_STRING);
		if ($prenom != '')
			$user->$nomBDDprenom = filter_var($prenom, FILTER_SANITIZE_STRING);
		$user->$nomBDDpassword = password_hash($password, PASSWORD_DEFAULT);
		$user->role_id = 1;
		$user->save();
	}

}
