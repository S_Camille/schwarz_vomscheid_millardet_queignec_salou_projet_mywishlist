<?php

namespace mywishlist\controllers;

use mywishlist\models\Utilisateur;
use mywishlist\views\GlobaleView;
use mywishlist\models\Liste;

class CreateurPubliqueController {

	public function __construct() {}

	public function affiche() {
		$head = GlobaleView::header([], 'Créateur Publique');
		$app = \Slim\Slim::getInstance();
		$foot = GlobaleView::footer();
		$html = $head;
		$listes = Liste::where('public', '=', 1)->get();
		$publique = array();
		foreach ($listes as $liste) {
			$utilisateur = $liste->utilisateur()->first();
			if (!in_array($utilisateur, $publique, false)) {
				$url = $app->urlFor('User', array('id'=>$utilisateur->user_id));
				$html = $html."<a href=\"$url\">".'<div class="utilisateur">'.$utilisateur->pseudo.'</div></a>';
				$publique[] = $utilisateur;
			}
		}
		$html = $html.$foot;
		echo $html;
	}

	public function initRendrePub() {
		$app = \Slim\Slim::getInstance();
		if (isset($_SESSION['user_connected'])) {
	    	$l = Liste::where('token', '=', $app->request->post('token'))->first();
			$p = $l->public;
			if ($p == 1) {
	    		$l->public = 0;
			}
			else {
	            $l->public = 1;
			}
			$l->save();
			$app->redirect($app->urlFor('ListeUser'));
		}
		$app->redirect($app->urlFor('Accueil'));
	}

}