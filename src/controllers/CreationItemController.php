<?php

namespace mywishlist\controllers;

use mywishlist\views\AjoutItemView;
use mywishlist\models\Item;

class CreationItemController {

	public function __construct() {}
		
	public function afficheAjoutElementListe() {
        $aff = new AjoutItemView();
		return $aff->render();
	}
	
	public static function ajout($no, $nom, $descr, $img, $url, $tarif) {
		$noFiltre=filter_var($no,FILTER_VALIDATE_INT);
		$nomFiltre=filter_var($nom, FILTER_SANITIZE_STRING);
		$descrFiltre=filter_var($descr, FILTER_SANITIZE_STRING);
		if ($url=='')
			$urlFiltre='';
		else
			$urlFiltre=filter_var($url, FILTER_VALIDATE_URL);
		if ($noFiltre !== false && $nomFiltre !== false && $descrFiltre !== false && $urlFiltre !== false) {
    	$it = new Item();
    	$it->liste_id = $noFiltre;
    	$it->nom = $nomFiltre;
    	$it->descr = $descrFiltre;
    	$it->img = $img;
    	$it->url = $urlFiltre;
    	$it->tarif = $tarif;
    	$it->save();
		}
	}

    public function initcreate() {
        $app = \Slim\Slim::getInstance();
        $no = $app->request->post('no');
        $token = $app->request->post('token');
        if (strlen($_FILES["imgItem"]["name"]) == 0) {
            $img = $app->request->post('urlImg');
        }
        else {
            $img = $_FILES["imgItem"]["name"];
        }
        CreationItemController::ajout($no, $app->request->post('nomItem'), $app->request->post('descrItem'),$img, $app->request->post('urlItem'), $app->request->post('tarifItem'));
        if ($_FILES["imgItem"]["name"] != null) {
            $res = move_uploaded_file($_FILES["imgItem"]["tmp_name"],"web/img/item/".$_FILES["imgItem"]["name"]);
        }
        $app->redirect($app->urlFor('Liste').'?no='.$token);
    }
    
}