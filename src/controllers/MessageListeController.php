<?php

namespace mywishlist\controllers;

use mywishlist\models\Liste;
use mywishlist\models\MessageListe;

class MessageListeController {
	
	private $liste_id;
	
	public function __construct($token) {
		$this->liste_id = Liste::where('token', '=', $token)->first()->no;
	}
	
	public function addMessListe() {
		$app = \Slim\Slim::getInstance();
		$request = $app->request;
		$boutonAddMess = $request->post('ajouter_messListe');
		if (isset($boutonAddMess) && $boutonAddMess == 'ajout_messListe') {
			$mess = new MessageListe();
			$mess->user_id = $_SESSION['user_connected']['user_id'];
			$mess->liste_id = $this->liste_id;
			$mess->message = filter_var($request->post('commentaire'), FILTER_SANITIZE_SPECIAL_CHARS);
			$mess->save();
		}	
	}

	public function initAjMesLis() {
		$app = \Slim\Slim::getInstance();
		if (isset($_SESSION['user_connected'])) {
			$this->addMessListe();
			$app->redirect($app->urlFor('Liste').'?no='.$_GET['token'].'#commentaires');
		}
		$app->redirect($app->urlFor('Accueil'));
	}

}