<?php

namespace mywishlist\controllers;

use mywishlist\models\Liste;
use mywishlist\views\ListeView;
use mywishlist\views\GlobaleView;

class AfficheListeController {
	
	public function __construct() {}

	public function affichageListes() {
		$app=\Slim\Slim::getInstance();
		$head = GlobaleView::header([], 'afficher liste');
		$foot = GlobaleView::footer();
		$liste1 = Liste::where('public', '=', 1)->first();
		$liste2 = Liste::where('public', '=', 1)->skip(1)->first();
		$liste3 = Liste::where('public', '=', 1)->skip(2)->first();
		echo $head.'<div class="ensemblesListes">';
		if ($liste1 != NULL) {
			echo $this->afficheListe($liste1);
		}
		if ($liste2 != NULL) {
			echo $this->afficheListe($liste2);
		}
		if ($liste3 != NULL) {
			echo $this->afficheListe($liste3);
		}
        $url = $app->urlFor('ListesPubliques');
        $url2 = $app->urlFor('CreatPublique');
        $url3 = $app->urlFor('ConsulterRes');
		echo <<<END
		</div><a href="$url" title="Afficher plus de listes publiques"><div class="lienPub" id="esc">Afficher plus de listes publiques</div></a>
		<a href="$url2" title="Afficher la liste des utilisateurs ayant une liste publique"><div class="lienPub">Afficher la liste des utilisateurs ayant une liste publique</div></a>
		<a href="$url3" title="Afficher la liste des réservations"><div class="lienPub">Afficher la liste des réservations</div></a>$foot
END;
	}
	
	public function afficheListe(Liste $l) {
		$app=\Slim\Slim::getInstance();
		$vl = new ListeView($l);
		$url = $app->urlFor('Liste').'?no='.$l->token;
    	return '<div class="liste">'.$vl->render_no()."<a href=\"$url\">".$vl->render_titre().'</a>'.$vl->render_desc().'</div>';
	}

}