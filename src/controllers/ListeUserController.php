<?php

namespace mywishlist\controllers;

use mywishlist\models\Liste;
use mywishlist\views\ListesView;
use mywishlist\views\GlobaleView;

class ListeUserController {
	
	public function __construct() {}

	public function affichageListes() {
		$head = GlobaleView::header([], 'Afficher Liste');
		$app = \Slim\Slim::getInstance();
		if (!isset($_SESSION['user_connected'])) {
			$app->redirect($app->urlFor('Accueil'));
		}
		$foot = GlobaleView::footer();
		if (isset($_SESSION['user_connected'])) {
			$listes = Liste::where('user_id' ,'=', $_SESSION['user_connected']['user_id'])->get();
			$urlCreaListe = $app->urlFor('Creation');
			$urlModificationCompte = $app->urlFor('ModificationCompte').'?err=0';
			echo $head;
			if (!$listes->isEmpty()) {
				echo '<div class="meslistes">'.$this->afficheListes($listes).'</div>';
			}
			echo '<a href="'.$urlCreaListe.'"><img  id="boutonAdd" src="web/img/ajouter.png" alt="Créer une liste" title="Créer une liste" width="125" height="125" /></a>';
			echo '<a href="'.$urlModificationCompte.'"><img  id="boutonAdd" src="web/img/modifier.png" alt="Modifier votre compte" title="Modifier votre compte" width="125" height="125" /></a>';
			echo $foot;
		}
	}
	
	public function afficheListes($l){
		$lv = new ListesView($l);
		return $lv->render();
	}
	
}