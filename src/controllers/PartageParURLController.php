<?php

namespace mywishlist\controllers;

use mywishlist\views\PartageParURLView;

class PartageParURLController {

	private $token;

	public function __construct($l) {
		$this->token = $l->token;
	}

	public function affichage() {
		$ppURL = new PartageParURLView($this->token);
		return $ppURL->bouton();
	}

}