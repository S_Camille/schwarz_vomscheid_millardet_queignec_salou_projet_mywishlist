<?php

namespace mywishlist\controllers;

use mywishlist\views\GlobaleView;
use mywishlist\models\Liste;
use mywishlist\models\Item;
use mywishlist\views\ListeView;
use mywishlist\views\ItemView;
use mywishlist\views\AjoutMessListeView;
use mywishlist\views\MessListeView;

class ListeEtItemController {

	private $n;

	public function __construct($no) {
		$this->n=$no;
	}

	public function affichageListe() {
		$app = \Slim\Slim::getInstance();
		$head = GlobaleView::header(['css1'=>'item.css'], 'Afficher liste');
		$foot = GlobaleView::footer();
		$liste = Liste::where('token' ,'=', $this->n)->first();
		$vl = new ListeView($liste);
		$items = Item::where('liste_id', '=', $liste->no)->get();
		$affItem = '<div class="items">';
		foreach ($items as $item) {
			$url = $app->urlFor('Item').'?id='.$item->id;
			$vi = new ItemView($item);
			$affItem = $affItem.'<div class="item">'.'<a href="'.$url.'">'.$vi->render_nom().'</a>'.$vi->render_img().'</div>';
		}
		$affItem = $affItem.'</div>';
		$al = new CreationItemController();
		$ppURL = new PartageParURLController($liste);
		$html = $head.'<div class="liste">'.$vl->render_no().$vl->render_titre().$vl->render_desc().'</div>'.$affItem;
		if (array_key_exists('user_connected', $_SESSION) != null) {
    		if ($_SESSION['user_connected']['user_id'] == $liste->user_id) {
        		$html = $html. $al->afficheAjoutElementListe().$ppURL->affichage();
		    }
        }
		$html = $html.AjoutMessListeView::render();
		$html = $html.MessListeView::render();
		$html = $html.$foot; 
		echo $html;
	}

}