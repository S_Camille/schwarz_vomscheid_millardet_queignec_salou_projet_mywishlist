<?php

namespace mywishlist\controllers;

use mywishlist\views\ConnexionView;
use mywishlist\views\GlobaleView;
use mywishlist\models\Role;

class ConnexionController {

	public function __construct() {}

	public function connecter() {
		$app = \Slim\Slim::getInstance();
		$user=Authentification::authentification('valider_connexion', 'valid_connexion', 'user', 'password', 'email', 'password', 'user_id');
		if ($user !== false) {
			$_SESSION['user_connected']['role_id']=$user->role_id;
			$_SESSION['user_connected']['auth_level']=Role::where('role_id', '=', $user->role_id)->first()->auth_level;
			$app->redirect($app->urlFor('Accueil'));
		}
		else {
			$app->redirect($app->urlFor('Connexion').'?err=1');
		}
	}
	
	public function affichePageCo(){
	    $aff = new ConnexionView();
	    $app = \Slim\Slim::getInstance();
		echo GlobaleView::header(['css1' => 'formulaire.css'], 'Se connecter');
		if (isset($_SESSION['user_connected'])) {
			$app->redirect($app->urlFor('Acceuil'));
		}
		echo $aff->render();
	}

}