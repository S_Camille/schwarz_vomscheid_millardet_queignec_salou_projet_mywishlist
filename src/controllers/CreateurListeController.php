<?php

namespace mywishlist\controllers;

use mywishlist\models\Liste;
use mywishlist\views\GlobaleView;
use mywishlist\views\CreateListView;

class CreateurListeController {

	public function __construct() {}

	public function afficheCreateur() {
		$clv = new CreateListView();
		$app = \Slim\Slim::getInstance();
		if (!isset($_SESSION['user_connected'])) {
			$app->redirect($app->urlFor('Accueil'));
		}
		echo $clv->render();
	}
	
	public static function ajoutListe($titre, $descr, $date) {
    	$liste = new Liste();
    	if ($titre != '' && $descr != '') {
            $liste->titre = filter_var($titre, FILTER_SANITIZE_STRING);
            $liste->description = filter_var($descr, FILTER_SANITIZE_STRING);
            $liste->expiration = $date;
			$liste->user_id = $_SESSION['user_connected']['user_id'];
			$liste->token = bin2hex(random_bytes(32));
			$liste->public = 0;
            $liste->save();
        }
        return $liste->token;
	}

	public function initajlis() {
		$app = \Slim\Slim::getInstance();
	    $titre = $app->request->post('titre');
	    $desc =  $app->request->post('description');
	    $date = $app->request->post('datetime-local');
	    $token = self::ajoutListe($titre, $desc , $date);
	    if ($token != -1) {
	        $app->redirect($app->urlFor('Liste').'?no='.$token);
	    }
	    else {
	        $app->redirect($app->urlFor('Accueil'));
	    }
	}

}