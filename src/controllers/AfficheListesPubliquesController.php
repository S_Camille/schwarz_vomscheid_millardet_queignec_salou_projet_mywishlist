<?php

namespace mywishlist\controllers;

use mywishlist\models\Liste;
use mywishlist\views\ListeView;
use mywishlist\views\GlobaleView;

class AfficheListesPubliquesController {

	public function __construct() {}

	public function affichageListes() {
		$head = GlobaleView::header([], 'afficher listes publiques');
		$foot = GlobaleView::footer();
		$html = $head;
		$al = new AfficheListeController();
		$listes = Liste::where('public', '=',1)->get();
		foreach ($listes as $l) {
		    $html = $html.$al->afficheListe($l)."<br />";
        }
		$html = $html.$foot;
		echo $html;
	}

}