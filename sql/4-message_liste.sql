DROP TABLE IF EXISTS `message-liste`;
CREATE TABLE `message-liste` (
  `mess_id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `liste_id` int(11) NOT NULL,
  `message` text NOT NULL,
  CONSTRAINT `fk_user_mess` FOREIGN KEY(`user_id`) REFERENCES `utilisateur`(`user_id`),
  CONSTRAINT `fk_liste_mess` FOREIGN KEY(`liste_id`) REFERENCES `liste`(`no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;