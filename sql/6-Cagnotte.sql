ALTER TABLE `item` ADD montCagn INT(11);
DROP TABLE IF EXISTS `coll-cagnotte`;
CREATE TABLE `coll-cagnotte` (
  `cagn_id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `montant` int(11) NOT NULL,
  `message` text NOT NULL,
  CONSTRAINT `fk_user_cagn` FOREIGN KEY(`user_id`) REFERENCES `utilisateur`(`user_id`),
  CONSTRAINT `fk_liste_cagn` FOREIGN KEY(`item_id`) REFERENCES `item`(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;