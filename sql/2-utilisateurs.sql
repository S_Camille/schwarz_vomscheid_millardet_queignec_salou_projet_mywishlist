DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE `utilisateur` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(60) NOT NULL,
  `pseudo` varchar(15) NOT NULL,
  `nom` text,
  `prenom` text,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `utilisateur` (`user_id`, `email`, `pseudo`, `password`) VALUES
(1, 'jacques.dupont@hotmail.com', 'Jacquouille', '$2y$10$q2DbN19bgepF8iXnHFCUYuRB.HSjZpTIcJZtWnifTHlTUTMa2hbva'), -- mot de passe : xMq347tX
(2, 'marc.durant@free.fr', 'Marcus96', '$2y$10$R98IIVrLmEfQdtYbvaIiu.18WkR9uTzXwM6/Wvong.hdHWG3piYQ2'), -- mot de passe : H9qKei64b9Rn2C
(3, 'loriedu112@gmail.com', 'Lorie112', '$2y$10$B4s0PCARb94jvIiS7ACSQeCMq7CHEQRgwKndGFXK6pBKAsEUAqHey'); -- mot de passe : L84u5eF

ALTER TABLE `liste`
ADD CONSTRAINT `liste_userid`
FOREIGN KEY(`user_id`)
REFERENCES `utilisateur`(`user_id`);