ALTER TABLE `utilisateur` DROP PRIMARY KEY, ADD PRIMARY KEY (`user_id`, `email`);

DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(60) NOT NULL,
  `auth_level` int(2) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `role` VALUES
(1, 'Créateur liste', 1),
(2, 'Modérateur', 2),
(3, 'Admin', 3);

ALTER TABLE `utilisateur` ADD`role_id` int(11) NOT NULL;

UPDATE `utilisateur` SET `role_id`=1;
ALTER TABLE `utilisateur` ADD CONSTRAINT `user_role` FOREIGN KEY(`role_id`) REFERENCES `role`(`role_id`);