DROP TABLE IF EXISTS `reservation`;
CREATE TABLE `reservation`(
	`id_reserv` int(11) NOT NULL AUTO_INCREMENT,
	`nom_reserv` text NOT NULL,
	`mess_reserv` text,
	PRIMARY KEY(id_reserv)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `item` ADD `id_reserv` int(11);

ALTER TABLE `item` ADD CONSTRAINT `resitem` FOREIGN KEY(`id_reserv`) REFERENCES `reservation`(`id_reserv`);