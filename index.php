<?php

require_once __DIR__ . '/vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;
use mywishlist\controllers\AfficheListeController;
use mywishlist\controllers\AfficheListesPubliquesUserController;
use mywishlist\controllers\AfficheListesPubliquesController;
use mywishlist\controllers\AccueilController;
use mywishlist\controllers\ConnexionController;
use mywishlist\controllers\DeconnexionController;
use mywishlist\controllers\ListeEtItemController;
use mywishlist\controllers\CreateurListeController;
use mywishlist\controllers\CreationItemController;
use mywishlist\controllers\ItemController;
use mywishlist\controllers\InscriptionController;
use mywishlist\controllers\ListeUserController;
use mywishlist\controllers\ModificationListeController;
use mywishlist\controllers\ModificationCompteController;
use mywishlist\controllers\SuppressionListeController;
use mywishlist\controllers\ModificationItemController;
use mywishlist\controllers\MessageListeController;
use mywishlist\controllers\SuppressionCompteController;
use mywishlist\controllers\UserController;
use mywishlist\controllers\CagnotteController;
use mywishlist\controllers\ReservController;
use mywishlist\controllers\ResImpoController;
use mywishlist\controllers\AjoutResController;
use mywishlist\controllers\ListeResController;
use mywishlist\controllers\ListeResPrivController;
use mywishlist\models\Item;
use mywishlist\models\Liste;
use mywishlist\models\Cagnotte;
use mywishlist\controllers\ConfirmerSuppressionController;
use mywishlist\models\Utilisateur;
use mywishlist\controllers\CreateurPubliqueController;

$tab = parse_ini_file('src/conf/conf.ini');
$db = new DB();
$db->addConnection($tab);
$db->setAsGlobal();
$db->bootEloquent(); 

session_start();
$app = new \Slim\Slim();

$app->get('/', function() {
	$acc = new AccueilController();
	$acc->affichageAcc();
})->name('Accueil');

$app->get('/liste', function() {
	$acc = new ListeEtItemController($_GET["no"]);
	$acc->affichageListe();
})->name('Liste');

$app->get('/item', function() {
	$ic = new ItemController($_GET["id"]);
	$ic->affichageItem();
})->name('Item');

$app->post('/itemBouton', function() {
    $appS = \Slim\Slim::getInstance();
	$appS->redirect($appS->urlFor('Liste')."?no=". $appS->request->post('token'));
})->name('ItemBouton');

$app->get('/creationListe', function() {
	$clc = new CreateurListeController();
	$clc->afficheCreateur();
})->name('Creation');

$app->post('/modifTitre', function(){
	$appS = \Slim\Slim::getInstance();
	$cl = new ModificationListeController($appS->request->post('no'));
	$cl->ModifierTitre($appS->request->post('titre'));
})->name('Titre');

$app->post('/modifDescr', function(){
	$appS = \Slim\Slim::getInstance();
	$cl = new ModificationListeController($appS->request->post('no'));
	$cl->ModifierDescr($appS->request->post('description'));
})->name('Descr');

$app->post('/modifDate', function(){
	$appS = \Slim\Slim::getInstance();
	$cl = new ModificationListeController($appS->request->post('no'));
	$cl->ModifierDate($appS->request->post('date'));
})->name('Date');

$app->post('/modifNom', function(){
	$appS = \Slim\Slim::getInstance();
	$cl = new ModificationCompteController($appS->request->post('id'));
	$cl->ModifierNom($appS->request->post('nom'));
})->name('NomProfil');

$app->post('/modifPrenom', function(){
	$appS = \Slim\Slim::getInstance();
	$cl = new ModificationCompteController($appS->request->post('id'));
	$cl->ModifierPrenom($appS->request->post('prenom'));
})->name('PrenomProfil');

$app->post('/modifEmail', function(){
	$appS = \Slim\Slim::getInstance();
	$cl = new ModificationCompteController($appS->request->post('id'));
	$cl->ModifierEmail($appS->request->post('email'));
})->name('EmailProfil');

$app->post('/suppressionCompte', function(){
	$app = \Slim\Slim::getInstance();
	$scc = new SuppressionCompteController($app->request->post('id'));
	$scc->affiche();
})->name('TrySupression');

$app->get('/createurPublique', function(){
	$cpc = new CreateurPubliqueController();
	$cpc->affiche();
})->name('CreatPublique');

$app->post('/suppressionTotaleCompte', function(){
	$app = \Slim\Slim::getInstance();
	$scc = new SuppressionCompteController($app->request->post('id'));
	$scc->initdelete();
})->name('SuppressionTotale');

$app->get('/confirmerSup', function(){
	$csc = new ConfirmerSuppressionController();
	$csc->afficher();
})->name('ConfirmerSup');

$app->post('/modificationMdp', function(){
	$appS = \Slim\Slim::getInstance();
	$cl = new ModificationCompteController($appS->request->post('id'));
	$cl->initmodmdp();
})->name('PasswordProfil');

$app->post('/ajoutListe', function(){
    $clc = new CreateurListeController();
    $clc->initajlis();
})->name('AjoutListe');

$app->get('/mesListes', function(){
	$cl = new ListeUserController();
	$cl->affichageListes();
})->name('ListeUser');

$app->post('/supItem', function(){
	$appS = \Slim\Slim::getInstance();
	$token = ItemController::supItem($appS->request->post('id'));
})->name('SupItem');

$app->get('/connect', function(){
	$cl = new ConnexionController();
	$cl->affichePageCo();
})->name('Connexion');

$app->post('/ajoutElement', function(){
    $cic = new CreationItemController();
    $cic->initcreate();
})->name('AjoutElement');

$app->get('/modifierCompte', function(){
	$mc = new ModificationCompteController(null);
	$mc->affichePageModification();
})->name('ModificationCompte');

$app->post('/connexionCompte', function(){
	$cc = new ConnexionController();
	$cc->connecter();
})->name('TryConnexion');

$app->get('/inscription', function(){
	$cl = new InscriptionController();
	$cl->affichePageInscr();
})->name('Inscription');

$app->post('/inscriptionCompte', function(){
	$cl = new InscriptionController();
	$cl->inscrire();
})->name('TryInscription');

$app->get('/disconnect', function(){
	$cl = new DeconnexionController();
	$cl->deconnecter();
})->name('Deconnexion');

$app->get('/modifListe', function(){
	$cl = new ModificationListeController($_GET['no']);
	$cl->affiche();
})->name('Modification');

$app->get('/supprListeConfirm', function(){
	$cl = new SuppressionListeController($_GET['no']);
	$cl->affiche();
})->name('SuppressionConfirme');

$app->post('/supprListe', function(){
	$appS = \Slim\Slim::getInstance();
	$cl = new SuppressionListeController($appS->request->post('nolistesuppr'));
	$cl->initsuplis();
})->name('SuppressionListe');

$app->post('/supprimerImageItem', function(){
	$appS = \Slim\Slim::getInstance();
	$ic = new ItemController($_GET['id']);
	$ic->supImgItem();
})->name('SupprimerImageItem');

$app->get('/modificationItem', function(){
    $mi = new ModificationItemController($_GET['id']);
	$mi->affiche();
})->name('ModifierItem');

$app->post('/modificationItem', function(){
    $appS = \Slim\Slim::getInstance();
    $ic = new ItemController($_GET['id']);
    $ic->initModItem();
})->name('EnregistrerModificationItem');

$app->post('/addMessListe', function(){
	$appS = \Slim\Slim::getInstance();
	$mc = new MessageListeController($_GET['token']);
	$mc->initAjMesLis();
})->name('AddMessListe');

$app->post('/rendrePublic', function(){
	$appS = \Slim\Slim::getInstance();
	$cpc = new CreateurPubliqueController();
	$cpc->initRendrePub();
})->name('RendrePublic');

$app->get('/listesPubliques', function(){
    $alp = new AfficheListesPubliquesController();
    $alp->affichageListes();
})->name('ListesPubliques');

$app->get('/listesPubliquesUser-:id', function($id){
    $alp = new AfficheListesPubliquesUserController($id);
    $alp->affichageListes();
})->name('ListesPubliquesUser');

$app->post('/participerCagnotte', function(){
	$appS = \Slim\Slim::getInstance();
	$cc = new CagnotteController($_GET['id']);
	$cc->initPartCagnotte();
})->name('ParticiperCagnotte');

$app->post('/modifierCagnotte', function(){
    $appS = \Slim\Slim::getInstance();
	$cc = new CagnotteController($_GET['id']);
	$cc->initModCagnotte();
})->name('ModifierCagnotte');

$app->post('/declencherCagnotte', function(){
    $appS = \Slim\Slim::getInstance();
	$cc = new CagnotteController($_GET['id']);
	$cc->initDecCagnotte();
})->name('DeclencherCagnotte');

$app->post('/annulerCagnotte', function(){
    $appS = \Slim\Slim::getInstance();
	$cc = new CagnotteController($_GET['id']);
	$cc->initAnnCagnotte();
})->name('AnnulerCagnotte');

$app->get('/user-:id', function($id){
    $uc=new UserController($id);
	$uc->afficher();
})->name('User');

$app->get('/reserv', function(){
	$rc = new ReservController($_GET['id']);
	$rc->afficheReservation();
})->name('Reserv');

$app->post('/reservAj', function(){
	$arc = new AjoutResController($_GET['no']);
	$arc->ajoutReserv();
})->name('AjoutRes');

$app->get('/reservImpo', function(){
	ResImpoController::affiche();
})->name('ResImpossible');

$app->get('/reservations', function() {
	$lrc = new ListeResController();
	$lrc->afficListeRes();
})->name('ConsulterRes');

$app->get('/reservationsPriv', function() {
	$lrc = new ListeResPrivController($_GET['no']);
	$lrc->afficListeRes();
})->name('ConsulterResPriv');

$app->run();